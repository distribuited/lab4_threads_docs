import java.io.IOException;

public class Converter implements Runnable {

    private static final int MAX_RETRIES = 3;
    private String url;
    private String outputPath;

    public Converter(String url) {
        this.url = url;
    }

    // override interface method run
    @Override
    public void run() {

        String localDir = System.getProperty("user.dir");
        String threadName = Thread.currentThread().getName();
        outputPath = localDir + "/pdf/";

        System.out.println("Thread " + threadName + " started."); // log

        if (url != null) {

            int retries = 0;
            boolean isConverted = false;

            // avoid infinite retries
            while (retries < MAX_RETRIES && !isConverted) {

                String[] command = {
                        "soffice",
                        "--convert-to",
                        "pdf",
                        "--outdir",
                        outputPath,
                        localDir + "/documents/" + url,
                        "--headless"
                };

                ProcessBuilder processBuilder = new ProcessBuilder(command);

                try {
                    Process process = processBuilder.start();
                    int exitCode = process.waitFor();
                    if (exitCode == 0) {
                        isConverted = true;
                        System.err.println("+++PDF generation for: " + url + " with thread: " + threadName);
                    } else {
                        System.err.println(
                                "---PDF generation failed for: " + url + " retry: #" + (retries + 1) + " with thread: "
                                        + threadName);
                    }
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }

                retries++;
            }

            // retries definitive fail
            if (!isConverted) {
                System.err.println("---PDF generation failed after " + MAX_RETRIES + " retries: " + url);
            }
        }

        System.out.println("Terminó de ejecutarse el thread" + threadName); // log
    }

    public String getUrl() {
        return url;
    }

    public String getOutputPath() {
        return outputPath;
    }
}
