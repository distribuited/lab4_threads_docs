import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class App {

    public static void main(String[] args) throws Exception {

        long startTime = System.currentTimeMillis();

        // gets all the documents location
        String folderPath = System.getProperty("user.dir") + "/documents/";
        File folder = new File(folderPath);
        File[] files = folder.listFiles();

        // threadpool to execute jobs concurrently
        int maxConcurrentJobs = 3;
        ExecutorService executor = Executors.newFixedThreadPool(maxConcurrentJobs); // queue jobs that exceed maxcjobs

        List<Converter> converters = new ArrayList<>();

        // submit sends a runnable job to execute it
        // job is schedule in one of threads of pool
        for (File file : files) {
            Converter converter = new Converter(file.getName());
            converters.add(converter);
            executor.submit(converter);
        }

        executor.shutdown();

        // blocks main program, until previous jobs be done
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("All conversions completed");

        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Total time: " + totalTime / 1000 + " seconds");

        for (Converter converter : converters) {
            System.out.println("URL: " + converter.getUrl() + " PDF location: " + converter.getOutputPath());
        }
    }
}
